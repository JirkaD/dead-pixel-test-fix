﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;

namespace DeadPixelTest
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            this.State["pivotIndex"] = this.Pivot.SelectedIndex;
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (this.State.ContainsKey("pivotIndex"))
                this.Pivot.SelectedIndex = (int)this.State["pivotIndex"];
            base.OnNavigatedTo(e);
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Color basicColor = ((SolidColorBrush)((Button)sender).Background).Color;
            NavigationService.Navigate(new Uri(string.Format("/ColorPage.xaml?red={0}&green={1}&blue={2}", basicColor.R, basicColor.G, basicColor.B), UriKind.Relative));
        }

        private void Repair_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            String text = ((Button)sender).Content.ToString();
            int mode = Int32.Parse(text.Substring(text.Length - 1));
            NavigationService.Navigate(new Uri(string.Format("/RepairPage.xaml?mode={0}", mode), UriKind.Relative));
        }

        private void Review_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var reviewTask = new MarketplaceReviewTask();
            reviewTask.Show();
        }
    }
}
