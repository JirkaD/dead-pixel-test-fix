﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace DeadPixelTest
{
    public partial class ColorPage : PhoneApplicationPage
    {
        public ColorPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            byte red = Byte.Parse(this.NavigationContext.QueryString["red"]);
            byte green = Byte.Parse(this.NavigationContext.QueryString["green"]);
            byte blue = Byte.Parse(this.NavigationContext.QueryString["blue"]);

            Color basicColor = Color.FromArgb(255, red, green, blue);
            this.LayoutRoot.Background = new SolidColorBrush(basicColor);

            //Color contrastColor = Color.FromArgb(255, (byte)(255 - red), (byte)(255 - green), (byte)(255 - blue));
            //this.ColorPageText.Foreground = new SolidColorBrush(contrastColor);
            if (red + green + blue < 255 * 3 / 2)
                this.ColorPageText.Foreground = new SolidColorBrush(Colors.White);
            else
                this.ColorPageText.Foreground = new SolidColorBrush(Colors.Black);
        }

        private void PhoneApplicationPage_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            TextDisappear.Begin();
        }

        private void LayoutRoot_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
        }
    }
}
