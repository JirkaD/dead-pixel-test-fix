﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace DeadPixelTest
{
    public partial class RepairPage : PhoneApplicationPage
    {
        int _mode;
        int _flashedCount = 0;

        public RepairPage()
        {
            InitializeComponent();

            Flash();
        }

        private void Flash()
        {
            if (_mode == 1)
            {
                switch (_flashedCount % 8)
                {
                    case 0:
                LayoutRoot.Background = new SolidColorBrush(Colors.Red);
                        break;
                    case 1:
                        LayoutRoot.Background = new SolidColorBrush(Colors.Cyan);
                        break;
                    case 2:
                        LayoutRoot.Background = new SolidColorBrush(Colors.Green);
                        break;
                    case 3:
                        LayoutRoot.Background = new SolidColorBrush(Colors.Magenta);
                        break;
                    case 4:
                        LayoutRoot.Background = new SolidColorBrush(Colors.Blue);
                        break;
                    case 5:
                        LayoutRoot.Background = new SolidColorBrush(Colors.Yellow);
                        break;
                    case 6:
                        LayoutRoot.Background = new SolidColorBrush(Colors.Black);
                        break;
                    case 7:
                        LayoutRoot.Background = new SolidColorBrush(Colors.White);
                        break;
                }
            }
            else
            {
                switch (_flashedCount % 2)
                {
                    case 0:
                        LayoutRoot.Background = new SolidColorBrush(Colors.Black);
                        break;
                    case 1:
                        LayoutRoot.Background = new SolidColorBrush(Colors.White);
                        break;
                }
            }

            var flashStoryboard = new Storyboard();
            flashStoryboard.Completed += new EventHandler((object sender, EventArgs e) => { Flash(); });
            flashStoryboard.Begin();

            _flashedCount++;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _mode = Byte.Parse(this.NavigationContext.QueryString["mode"]);
        }
    }
}
